package model;

import java.io.Serializable;
/**
 * Classa pre administraciu procesov vozidiel
 */
public abstract class Vehicle implements Serializable {
	private int suma;
	public boolean available = true;
	public int speed;

	public int getSuma() {
		return suma;
	}

	public void setSuma(int suma) {
		this.suma = suma;
	}

	public boolean nalozVozidlo(int suma) {
		if (suma >= 1000 && suma < 200000) {
			this.setSuma(suma);
			return true;
		}
		return false;
	}

	public boolean vysliVozidloAut(Bankomat bankomat) {
		bankomat.akceptujPeniaze(this);
		return true;
	}
}
