package model;
/**
 * odvodena classa Vehicle pre mensie prevody
 */
public class SmallVehicle extends Vehicle {

	public SmallVehicle(int suma) {
		this.setSuma(suma);
		this.speed = 100 + 20000 / suma;
		System.out.println(this.speed);
	}
}
