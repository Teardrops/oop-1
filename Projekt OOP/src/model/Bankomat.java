package model;

import java.io.Serializable;

import controller.Transport;
import controller.User;
/**
 * classa zaoberajuca sa pracou s bankomatmi. 
 */
public class Bankomat implements Serializable {

	private static final long serialVersionUID = 1L;
	private int hodnota;
	protected int pocetOperacii;
	public boolean vPrevadzke = false;

	public int getHodnota() {
		return hodnota;
	}

	public void setHodnota(int hodnota) {
		this.hodnota = hodnota;
	}

	public int getPocetOperacii() {
		return pocetOperacii;
	}

	public void setPocetOperacii(int pocetOperacii) {
		this.pocetOperacii = pocetOperacii;
	}
/**
 * @ param bank bank - zakladna entita projektu bank
 * @ param int suma - suma ktoru ma bankomat vydať
 * @ param user pouzivatel - pouzivatel ktory sa pokusa o výber
 * @ return boolean , 0 peniaze neboli vydane , 1 boli vydane
 */
	public boolean vydajPenazi(Bank bank, int suma, User pouzivatel) {
		if (suma < this.hodnota) {
			pouzivatel.kredit -= suma;
			this.hodnota -= suma;
			this.setPocetOperacii(this.getPocetOperacii() + 1);
			System.out.println("Vydane:" + suma);
			if (this.hodnota < 1000)
				this.vyziadajPeniaze(bank, 15000);
			return true;
		}
		return false;
	}
/**
 * abstraktna funkcia ktora vylozi celu hodnotu vozidla do bankomatu
 */
	public void akceptujPeniaze(Vehicle vozidlo) {
		this.hodnota += vozidlo.getSuma();
		vozidlo.setSuma(0);
	}
/**
 * funkcia na vyziadanie penazi z penazi 
 * @ param bank bank - zakladna entita programu
 * @ param int suma - suma ktoru si bankomat ziada
 * @ return boolean - ak by doslo k nepredpokladanej chybe pri nakladani vozidla false signalizuje chybu
 */
	public boolean vyziadajPeniaze(Bank bank, int suma) {
		bank.vozidla.add(new Transport(new SmallVehicle(suma)));
		Transport a = bank.vozidla.get(bank.vozidla.size() - 1);
		if (a.vozidlo.nalozVozidlo(suma)) {
			a.vozidlo.vysliVozidloAut(this);
			bank.vozidla.remove(a);
			return true;
		}
		return false;
	}
}
