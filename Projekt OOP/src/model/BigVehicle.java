package model;
/**
 * odvodena classa Vehicle pre vacsie prevody
 */
public class BigVehicle extends Vehicle {

	public BigVehicle(int suma) {
		this.setSuma(suma);
		this.speed = 100 + 200000 / suma;
		System.out.println(this.speed);
	}
}
