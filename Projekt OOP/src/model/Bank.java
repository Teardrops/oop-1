package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import controller.Transport;
import controller.User;
/**
 * hlavna entita projektu 
 */
public class Bank implements Serializable {

	public ArrayList<Transport> vozidla = new ArrayList<Transport>();;
	public ArrayList<User> users = new ArrayList<User>();
	public Bankomat[] bankomaty;
/**
 * serializacia
 */
	public void serialize() {
		try {
			FileOutputStream fileOut = new FileOutputStream("banka.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(this);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
/**
 * deserializacia
 */
	public void load() {
		Bank bank = null;
		try {
			FileInputStream fileIn = new FileInputStream("banka.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bank = (Bank) in.readObject();
			this.bankomaty = bank.bankomaty;
			this.users = bank.users;
			this.vozidla = bank.vozidla;
			in.close();
			fileIn.close();
			return;
		} catch (IOException i) {
			i.printStackTrace();
			return;
		} catch (ClassNotFoundException c) {
			System.out.println("Chyba, neexistujúca classa");
			c.printStackTrace();
			return;
		}
	}
}
