package view;
/**
 * class zaoberajuca sa GUI aplikacie
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import javax.swing.colorchooser.DefaultColorSelectionModel;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.*;
import controller.*;

public class BankGui extends Application {
	public Scene scene;
	public ButtonFuctions buttonhelp = new ButtonFuctions();
	private Button home = new Button("Home");
	private Button back = new Button("Back");
	private Button login = new Button("Login");
	private Button buttonf1 = new Button("Vyber Penazi");
	private Button buttonf2 = new Button("Vysli vozidlo");
	private Button buttonf3 = new Button("Stav Bankomatu");
	private Button buttonf4 = new Button("Stav Banky");
	private Button buttonf20 = new Button("Vysli Vozidlo");
	private Button notifyb = new Button("Zrus");
	private Label label1 = new Label("Name:");
	private Label labelV = new Label("");
	private Label labelB = new Label("");
	private Label labelB2 = new Label("");
	private Label total = new Label("Total");
	private Label message = new Label("");
	private Label konto = new Label("");
	private Alert alert = new Alert(AlertType.CONFIRMATION);
	private TextField textField = new TextField();
	private TextField refillField = new TextField();
	public TextField ttF = new TextField();
	private PasswordField pwF = new PasswordField();
	public Bank bank = new Bank();
	private StackPane root = new StackPane();
	public User loggedIn = null;
	public int j = 0;
	public int[] x = new int[] { 25, 215, 120 };
	public int[] y = new int[] { 85, 85, 270 };
	private Object[] stavy = null;
	public ArrayList<Node> scene0 = new ArrayList<Node>();
	public ArrayList<Node> scene1 = new ArrayList<Node>();
	public ArrayList<Node> scene2 = new ArrayList<Node>();
	public ArrayList<Node> scene3 = new ArrayList<Node>();
	public ArrayList<Node> scene4 = new ArrayList<Node>();
	public ChoiceBox<String> cb = new ChoiceBox<String>(
			FXCollections.observableArrayList("Bankomat 0", "Bankomat 1", "Bankomat 2"));
	public Canvas canvas = new Canvas(300, 300);
	public GraphicsContext gc = canvas.getGraphicsContext2D();
/**
 * prihlasovanie pouzivatela a nacitanie prislusneho zobrazenie pre konkretnych Userov
 */
	public void sceneLogin(User user) {
		System.out.println("SceneLogin sucessfull");
		buttonf1.setTranslateX(-200);
		buttonf1.setTranslateY(-200);
		buttonf2.setTranslateX(-200);
		buttonf2.setTranslateY(-170);
		buttonf3.setTranslateX(-200);
		buttonf3.setTranslateY(-140);
		buttonf4.setTranslateX(-200);
		buttonf4.setTranslateY(-110);
		message.setTranslateY(-100);
		root.getChildren().clear();
		root.getChildren().addAll(home);
		home.setText("Logout");
		if (user instanceof AdminUser) {
			System.out.println("ADMIN Logged IN");
			root.getChildren().addAll(scene1);
		} else if (user instanceof ManagerUser) {
			System.out.println("MANAGER Logged IN");
			root.getChildren().addAll(scene1);
			root.getChildren().remove(buttonf4);
		} else if (user instanceof BankUser) {
			System.out.println("BANK Logged IN");
			root.getChildren().addAll(scene1);
			root.getChildren().removeAll(buttonf3, buttonf4);
		} else if (user instanceof BasicUser) {
			System.out.println("BASIC Logged IN");
			root.getChildren().addAll(scene1);
			root.getChildren().removeAll(buttonf2, buttonf3, buttonf4);
		} else {
			restart();
			message.setText("Nespravne meno alebo heslo");
			return;
		}
		konto.setText(loggedIn.name + " | " + Integer.toString(loggedIn.kredit) + "€");
		ttF.setMaxWidth(210);
	}
/**
 * funkcia pre znovuzobrazenie defaultneho GUI
 */
	public void restart() {
		if (root.getChildren().contains(cb)) {
			cb.setValue(null);
		}
		root.getChildren().clear();
		root.getChildren().addAll(scene0);
		home.setText("Home");
		message.setText("");
		konto.setText("");
		labelB.setText("");
		total.setText("Total");
		loggedIn = null;
	}
/**
 * funckia pre zobrazenie predoslej sceny
 */
	public void back() {
		if (root.getChildren().contains(cb)) {
			cb.setValue(null);
		}
		sceneLogin(loggedIn);
		message.setText("");
		labelB.setText("");
		labelB2.setText("");
		total.setText("Total");
		refillField.setText("");
		ttF.setText("");
	}
/**
 * funkcia button handleru pre button vyber
 */
	public void vyberPeniaze(User user, Stage primaryStage) {
		int temp = 0;
		if (!root.getChildren().contains(labelV)) {
			root.getChildren().add(labelV);
			labelV.setTranslateY(-75);
		}
		System.out.println("Inicializujem Vyber");
		int suma = 0;
		try {
			suma = Integer.parseInt(ttF.getText());
		} catch (Exception e) {
			labelV.setText("Zadajte ciselnu hodnotu");
			ttF.clear();
			return;
		}
		ttF.setText("");
		System.out.println(suma);
		temp = user.vyberPeniaze(suma);
		if (temp == 2) {
			for (Bankomat s : bank.bankomaty) {
				if (s.vPrevadzke && s.getHodnota() > suma) {
					System.out.println("Mam bankomat ktory bude OK");
					if (s.vydajPenazi(bank, suma, user)) {
						System.out.println("Vybral som");
						labelV.setText("Vyber Uspesny");
						konto.setText(loggedIn.name + " | " + Integer.toString(loggedIn.kredit) + "€");
					}
					return;
				}
			}
			labelV.setText("Vyber Neuspesny -> Nedostatok Prostriedkov v Bankomate");
		} else if (temp == 0)
			labelV.setText("Vyber Neuspesny -> Prekrocenie Limitu");
		else if (temp == 1)
			labelV.setText("Vyber Neuspesny -> Nedostatok Prostriedkov na Ucte");
		textField.setText("");

	}
/**
 * funkcia pre handler buttonu stav bankomatu
 */
	public void stavBankomatu() {
		root.getChildren().clear();
		root.getChildren().addAll(scene2);
		root.getChildren().addAll(home, back);
	}
/**
 * funkcia pre handler button stav banky
 */
	public void stavBanky() {
		root.getChildren().clear();
		root.getChildren().addAll(scene3);
		root.getChildren().addAll(home, back);
		int tot = 0;
		int b = 0;
		for (int c = 0; c < stavy.length; c++) {
			if ((!((Label) stavy[c]).getText().equals("Total")) && stavy[c] instanceof Label) {
				((Label) stavy[c])
						.setText("Bankomat" + (c - 1) + " | " + Integer.toString(bank.bankomaty[b].getHodnota()) + "€");
				tot += bank.bankomaty[b].getHodnota();
				b++;
			}
		}
		total.setText("Celkova hodnota banky je: " + tot + "€");
	}
/**
 * funkcia pre button handler posli vozidlo
 */
	public void posliVozidlo() {
		root.getChildren().clear();
		root.getChildren().addAll(scene4);
		root.getChildren().addAll(home, back);
		gc.strokeOval(0, 60, 30, 30);
		gc.strokeOval(210, 60, 30, 30);
		gc.strokeOval(105, 270, 30, 30);
		gc.setFill(Color.BLACK);
		gc.fillOval(105, 150, 30, 30);
	}
/**
 * funkcia pre button handler posli v posli vozidlo
 */
	public void posli() {
		if (!root.getChildren().contains(labelV)) {
			root.getChildren().add(labelV);
			labelV.setTranslateY(-175);
		}
		int suma = 0;
		String name = "" + j;
		Transport i = null;
		try {
			suma = Integer.parseInt(refillField.getText());
		} catch (Exception e) {
			labelV.setText("Zadajte ciselnu hodnotu");
			refillField.clear();
			return;
		}
		if (suma < 1000) {
			labelV.setText("Minimalna suma na prenos je 1000");
			refillField.clear();
			return;
		}
		if (cb.getValue() != null) {
			labelV.setText("");
			if (suma > 10000)
				bank.vozidla.add(new Transport(new BigVehicle(suma), gc, name,
						x[cb.getSelectionModel().getSelectedIndex()], y[cb.getSelectionModel().getSelectedIndex()],loggedIn,bank,cb.getSelectionModel().getSelectedIndex(),labelV));
			else
				bank.vozidla.add(new Transport(new SmallVehicle(suma), gc, name,
						x[cb.getSelectionModel().getSelectedIndex()], y[cb.getSelectionModel().getSelectedIndex()],loggedIn,bank,cb.getSelectionModel().getSelectedIndex(),labelV));
			i = bank.vozidla.get(bank.vozidla.size() - 1);
			if (i.vozidlo.nalozVozidlo(suma)) {
				i.start();
				labelV.setText("Doplnenie Bankomatu" + cb.getSelectionModel().getSelectedIndex()
						+ " bolo uspesne zahajene: Suma: " + suma);
			} else {
				labelV.setText("Chyba pri nakladani vozidla");
			}
			refillField.clear();
			cb.setValue(null);
			j++;
		} else {
			labelV.setText("Vyberte Bankomat");
			return;
		}
	}
	/**
	 * notify funkcia pre zoznam transportov ktory zastavi vsetky transport a zaroven funkcia
	 * button handleru pre zrus 
	 */
	public void zastav() { // notify transport
		for (Transport a: bank.vozidla)
		{
			System.out.println(a.getId());
			a.zastav();
		}
		bank.vozidla.clear();
	}
/**
 * predeklaracia scen a inych veci v GUI
 */
	public void predec() {
		scene0.add(label1);
		scene0.add(textField);
		scene0.add(pwF);
		scene0.add(message);
		scene0.add(home);
		scene0.add(login);
		scene1.add(buttonf1);
		scene1.add(buttonf2);
		scene1.add(buttonf3);
		scene1.add(buttonf4);
		scene1.add(ttF);
		scene1.add(konto);
		scene2.add(cb);
		scene2.add(labelB);
		scene2.add(labelB2);
		scene3.add(total);
		scene4.add(canvas);
		scene4.add(cb);
		scene4.add(refillField);
		scene4.add(buttonf20);
		scene4.add(notifyb);
		bank.load();
		/*
		 User jozko = new BasicUser("Jozko", "love", 12000); User jozko2 = new
		 ManagerUser("Jozko2", "love2", 120000); User admin = new
		 AdminUser("Admin", "admin", 2000000); bank.users.add(jozko);
		 bank.users.add(jozko2); bank.users.add(admin); bank.bankomaty = new
		 Bankomat[3];
		 
		for (int b = 0; b < 3; b++) {
			bank.bankomaty[b] = new Bankomat();
			bank.bankomaty[b].vPrevadzke = true;
		}
		bank.bankomaty[0].setHodnota(10000);
		bank.bankomaty[1].setHodnota(11111);
		bank.bankomaty[2].setHodnota(123456);*/
		System.out.println(bank.bankomaty[bank.bankomaty.length - 1].getHodnota());
		final Label[] stav = new Label[bank.bankomaty.length];
		for (int c = 0; c < stav.length; c++) {
			stav[c] = new Label();
			stav[c].setMinWidth(150);
			stav[c].setTranslateY(-125 + c * 15);
			scene3.add(stav[c]);
		}
		total.setTranslateY(-125 + stav.length * 15);
		stavy = scene3.toArray();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		predec();
		pwF.setPromptText("Your password");
		textField.setPromptText("Your Name");
		refillField.setPromptText("Zadajte sumu");
		ttF.setPromptText("Zadajte sumu");
		alert.setTitle("Logout");
		alert.setHeaderText("Naozaj sa chces odhlasit?");
		alert.setContentText("Naozaj?");
		ButtonType buttonTypeOne = new ButtonType("ANO");
		ButtonType buttonTypeCancel = new ButtonType("NIE", ButtonData.CANCEL_CLOSE);

		alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeCancel);

		textField.setMaxWidth(210);
		refillField.setMaxWidth(150);
		refillField.setTranslateY(-150);
		notifyb.setTranslateX(200);
		notifyb.setTranslateY(-120);
		buttonf20.setTranslateY(-120);
		pwF.setMaxWidth(210);
		ttF.setMaxWidth(210);
		login.setTranslateY(100);
		message.setText("");
		pwF.setTranslateY(50);
		label1.setTranslateY(50);
		message.setTranslateY(125);
		home.setTranslateX(200);
		home.setTranslateY(-150);
		back.setTranslateX(145);
		back.setTranslateY(-150);
		konto.setTranslateX(180);
		konto.setTranslateY(-175);
		cb.setTranslateX(-150);
		cb.setTranslateY(-150);
		labelB.setTranslateX(-150);
		labelB2.setTranslateX(-150);
		labelB2.setTranslateY(-25);
		labelV.setTranslateY(-75);
		cb.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			public void changed(ObservableValue ov, Number value, Number new_value) {
				if ((int) new_value != -1)
				{
					labelB.setText("Bankomat:" + new_value.intValue() + " | "
							+ bank.bankomaty[new_value.intValue()].getHodnota() + "€");
					labelB2.setText("Pocet operacii od posledneho pristupu: "+ bank.bankomaty[new_value.intValue()].getPocetOperacii());
					bank.bankomaty[new_value.intValue()].setPocetOperacii(0);
				}
			}
		});
		home.setOnAction((e) -> {
			if (loggedIn == null)
				restart();
			else {
				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == buttonTypeOne) {
					restart();
				}
			}
		});
		back.setOnAction((e) -> back());
		buttonf1.setOnAction((e) -> vyberPeniaze(loggedIn, primaryStage));
		buttonf2.setOnAction((e) -> posliVozidlo());
		buttonf3.setOnAction((e) -> stavBankomatu());
		buttonf4.setOnAction((e) -> stavBanky());
		buttonf20.setOnAction((e) -> posli());
		notifyb.setOnAction((e) -> zastav());
		login.setOnAction((e) -> {
			loggedIn = buttonhelp.checkLogin(pwF, textField, bank, loggedIn);
			sceneLogin(loggedIn);
		});
		primaryStage.setOnHiding((e) -> {
			bank.serialize();
		});
		root.getChildren().addAll(scene0);
		root.setStyle("-fx-background-color: #C0C0C0");
		scene = new Scene(root, 600, 600);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
