package controller;

import model.Bankomat;
import model.Vehicle;
/**
 * odvodena classa 2 stupna User
 */
public class BankUser extends BasicUser {

	public boolean vysliVozidloMan(Bankomat bankomat, Vehicle vozidlo) {
		bankomat.akceptujPeniaze(vozidlo);
		return true;
	}
	public int vyberPeniaze(int suma) {
		if (suma > 5000 || suma < 0)
			return 0;
		if (suma <= kredit)
			return 2;
		else
			return 1;
	}
}
