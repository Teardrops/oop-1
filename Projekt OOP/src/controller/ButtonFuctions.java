package controller;

import controller.User;
import model.Bank;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
/**
 * pomocna trieda pre login userov
 */
public class ButtonFuctions {
/**
 *  funkcia na skontrolovanie loginu, vrati prihlaseneho pouzivatela
 */
	public User checkLogin(PasswordField pwF, TextField textField, Bank bank, User user) {
		System.out.println("Hladam usera");
		for (User s : bank.users) {
			if (textField.getText().equals(s.name)) {
				if (s.login(pwF)) {
					System.out.println("LOGIN APPROVED");
					user = s;
					pwF.clear();
					textField.clear();
					return user;
				}
			}
		}
		System.out.println("KONIEC LOGINU");
		pwF.clear();
		textField.clear();
		return user;
	}

}
