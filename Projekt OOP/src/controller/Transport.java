package controller;


import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import model.Bank;
import model.Vehicle;
/**
 * class pre vykonavanie transportov
 */
public class Transport extends Thread  {

	private String ID;
	private Thread t;
	public Vehicle vozidlo;
	public GraphicsContext gc;
	private double x;
	private double y;
	private int a = 1;
	public int suma;
	public User loggedIn;
	public Bank bank;
	public int root;
	public Label sign;
	

	public Transport(Vehicle vozidlo, GraphicsContext gc, String ID, int x, int y,User loggedIn,Bank bank,int root,Label label) {
		this.vozidlo = vozidlo;
		this.gc = gc;
		this.ID = ID;
		this.x = x;
		this.y = y;
		this.bank = bank;
		this.loggedIn = loggedIn;
		this.root = root;
		this.sign = label;
	}

	public Transport(Vehicle vozidlo) {
		this.vozidlo = vozidlo;
	}

	public void run() {
		int a = 1;
		System.out.println("Running " + ID + " " + vozidlo.speed);
		try {
			while (a < this.vozidlo.speed) {
				Platform.runLater(() -> {
					update();
				});
				a++;
				if (!(Thread.currentThread().getName().equalsIgnoreCase("JavaFX Application Thread")))
					Thread.sleep(100);
			}
			gc.clearRect(0, 0, 300, 300);
			gc.strokeOval(0, 60, 30, 30);
			gc.strokeOval(210, 60, 30, 30);
			gc.strokeOval(105, 270, 30, 30);
			gc.setFill(Color.BLACK);
			gc.fillOval(105, 150, 30, 30);
			((BankUser) loggedIn).vysliVozidloMan(bank.bankomaty[this.root],
					this.vozidlo);
			bank.bankomaty[this.root].setPocetOperacii(bank.bankomaty[this.root].getPocetOperacii()+1);
			Platform.runLater(() -> {
				this.sign.setText("Bankomat" + root + " bol uspesne doplneny");
			});
			bank.vozidla.remove(this);
		} catch (InterruptedException e) {
			System.out.println("Thread " + ID + " interrupted.");
		}
		System.out.println("Thread " + ID + " exiting.");
	}

	public void start() {
		System.out.println("Starting thread: " + this.ID);
		if (t == null) {
			t = new Thread(this, this.ID);
			t.start();
		}
	}
/**
 * vykreslovanie cesty transportu
 */
	public synchronized void kresli() {
		gc.setStroke(Color.BLACK);
		gc.strokeLine(120, 165, 120 + (this.x - 120) / this.vozidlo.speed * this.a,
				165 + (this.y - 165) / this.vozidlo.speed * this.a);
		this.a++;
		System.out.println(a + "Thread:" + Thread.currentThread().getName());
	}

	public synchronized void update() {
		kresli();
	}
/**
 * zastavenie vykreslovania
 */
	public void zastav(){
		System.out.println(this.t.getId());
		this.t.interrupt();
		gc.clearRect(0, 0, 300, 300);
		gc.strokeOval(0, 60, 30, 30);
		gc.strokeOval(210, 60, 30, 30);
		gc.strokeOval(105, 270, 30, 30);
		gc.setFill(Color.BLACK);
		gc.fillOval(105, 150, 30, 30);
		Platform.runLater(() -> {
			this.sign.setText("Vsetky transfery zrusene!");
		});
	}
}
