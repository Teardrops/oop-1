package controller;

import java.io.Serializable;

import javafx.scene.control.PasswordField;

/**
 * abstraktna classa od ktorej sa odvodzuju ostatne User classy.
 * obsahuje funkconalitu login pre vsetkych Userov a zakladnu metodu pre vyber
 * penazi.
 */
public abstract class User implements Serializable {

	private static final long serialVersionUID = 1L;
	public String name;
	public String password;
	public int kredit;

	public User(String name, String password, int kredit) {
		this.name = name;
		this.password = password;
		this.kredit = kredit;
	}

	public User() {

	}

	public boolean login(PasswordField password) {
		if (password.getText().equals(this.password))
			return true;
		return false;
	}
/**
 * @param int suma - suma na vyber
 * @return int 0 = zly vyber prekrocenie limitu 2 = prekrocenie kreditu 1 = vsetko prebehlo v poriadku
 */
	public int vyberPeniaze(int suma) {
		if (suma > 0)
			return 0;
		if (suma <= kredit)
			return 2;
		else
			return 1;
	}
}
