package controller;
/**
 * odvodena classa 1 stupna User
 */
public class BasicUser extends User {

	public BasicUser(String name, String password, int kredit) {
		this.name = name;
		this.password = password;
		this.kredit = kredit;
	}

	public BasicUser() {

	}

	public int vyberPeniaze(int suma) {
		if (suma > 2500 || suma < 0)
			return 0;
		if (suma <= this.kredit)
			return 2;
		else
			return 1;
	}
}