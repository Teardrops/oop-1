package controller;

import model.Bankomat;
/**
 * odvodena classa 3 stupna User
 */
public class ManagerUser extends BankUser {
	public ManagerUser(String name, String password, int kredit) {
		this.name = name;
		this.password = password;
		this.kredit = kredit;
	}

	public ManagerUser() {

	}

	public int vyberPeniaze(int suma) {
		if (suma > 10000 || suma < 0)
			return 0;
		if (suma <= kredit)
			return 2;
		else
			return 1;
	}
}
