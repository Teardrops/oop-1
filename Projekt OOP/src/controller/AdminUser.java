package controller;
/**
 * odvodena classa 4 stupna User
 */
public class AdminUser extends ManagerUser {
	public AdminUser(String name, String password, int kredit) {
		this.name = name;
		this.password = password;
		this.kredit = kredit;
	}

	public void stavBanky() {

	}

	public int vyberPeniaze(int suma) {
		if (suma > 25000 || suma < 0)
			return 0;
		if (suma <= kredit)
			return 2;
		else
			return 1;
	}
}
